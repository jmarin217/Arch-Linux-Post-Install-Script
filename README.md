# *I AM NOT RESPONSIBLE FOR ANYTHING THAT HAPPENS TO YOUR INSTALL*
# ARCH LINUX POST INSTALL SCRIPT

This is a script I have written that will install essential (in my opinion) programs 
including Xorg, a browser, a window manager (i3), and an AUR helper.

# HOW TO USE
- Step one: Run "sh ./main.sh"
- Step two: Enter your sudo password when prompted.
- Step three: Grab a coffee!
- Step four: Profit!

# Dependencies
- base-devel

# This *does not* do...
- This does not create a user for you.
