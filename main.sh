#!/usr/bin/env bash
echo 'Getting Xorg working, as well as lightdm and configuring that (as far as selecting a greeter)...'
sudo pacman -Syu
sudo pacman -S --noconfirm xorg-xinit xorg-server xorg
sudo pacman -S --noconfirm lightdm lightdm-gtk-greeter
sudo pacman -S --noconfirm i3 xfce4-goodies
echo 'Browser selection...'
PS3='What browser would you like?: '
options=("Chromium" "Firefox" "No browser")
select opt in "${options[@]}"
do
    case $opt in
        "Chromium")
            sudo pacman -S --noconfirm chromium
            ;;
        "Firefox")
            sudo pacman -S --noconfirm firefox
            ;;
        "No browser")
            break
            ;;
        *) echo Please try again.;;
    esac
done
break
sudo pacman -S --noconfirm networkmanager network-manager-applet
sudo pacman -S --noconfirm pulseaudio
sudo pacman -S --noconfirm git
sudo pacman -S --noconfirm wget
echo 'Configuring lightdm...'
sudo echo "greeter-session=lightdm-gtk-greeter" >> /etc/lightdm/lightdm.conf
echo 'Getting "Trizen" an AUR helper...'
mkdir 'AUR Helper'
cd 'AUR Helper'
wget https://aur.archlinux.org/cgit/aur.git/snapshot/trizen.tar.gz
tar -xvf trizen.tar.gz
cd trizen
makepkg --noconfirm -si
echo 'Installing LibreOffice...'
sudo pacman -S --noconfirm libreoffice-fresh
echo 'Enabling services...'
sudo systemctl enable lightdm
sudo systemctl enable NetworkManager
echo 'Cleaning up...'
cd ~/
rm -r 'AUR Helper'
## Thank you message
echo "Thank you for using this script! Enjoy your new OS."
done
